import {Component, OnInit} from '@angular/core';
import {_HttpClient} from '@delon/theme';
import {NzMessageService, NzModalRef} from 'ng-zorro-antd';

@Component({
  selector: 'app-isystem-annount-add',
  templateUrl: './annount-add.component.html',
})
export class IsystemAnnountAddComponent implements OnInit {
  record: any = {};
  i: any={};
  constructor(
    private modal: NzModalRef,
    private msgSrv: NzMessageService,
    public http: _HttpClient,
  ) { }

  ngOnInit(): void {
    
  }

  save(value: any) {
    this.http.post(`sys/annountCement/add`, this.i).subscribe(res => {
      this.modal.close(true);
    });
  }

  close() {
    this.modal.destroy();
  }
}
